package elf

import (
	"bytes"
	"encoding/binary"
	"errors"
	"io"
	"io/ioutil"
	"log"
)

// Nagłówek ELF służy do przechowywania informacji dotyczących organizacji pliku
type Header struct {
	Ident     [4]uint32 // identyfikator pliku oraz inne dodatkowe informacje
	Type      uint16    // identyfikator typu obiektu
	Machine   uint16    // identyfikator architektury skompilowanego pliku
	Version   uint32    // identyfikator wersji pliku
	Entry     uint32    // wirtualny adres startowy wykonywania programu
	Phoff     uint32    // offset w pliku wskazujący na początek tablicy nagłówków programu
	Shoff     uint32    // offset w pliku wskazujący na początek tablicy nagłówków sekcji
	Flags     uint32    // specyficzne flagi procesora
	Ehsize    uint16    // wielkość nagłówku ELF w bajtach
	Phentsize uint16    // wielkość jednego nagłówku programu w tablicy nagłówków programu
	Phnum     uint16    // liczba nagłówków programu w tablicy nagłówków programu
	Shentsize uint16    // wielkość jednego nagłówku sekcji w tablicy nagłówków sekcji
	Shnum     uint16    // liczba nagłówków sekcji w tablicy nagłówków sekcji
	Shstrndx  uint16    // indeks w tablicy nagłówków sekcji, określający nagłówek sekcji stringów w pliku
}

// Nagłówek sekcji służy do przechowywania informacji dotyczących sekcji programu
type Section struct {
	Name      uint32 // nazwa sekcji, jako indeks, wskazujący na string, zakończony zerem w sekcji stringów
	Type      uint32 // typ sekcji
	Flags     uint32 // flagi sekcji
	Addr      uint32 // adres wirtualny pod którym w procesie zostanie umieszczony pierwszy bajt danej sekcji
	Offset    uint32 // offset w pliku na początek danej sekcji
	Size      uint32 // rozmiar sekcji w bajtach
	Link      uint32 // link indeksowy na tablice nagłówków sekcji
	Info      uint32 // dodatkowa informacja, dotycząca aktualnej sekcji
	Addralign uint32 // wielkość wyrównywania sekcji
	Entsize   uint32 // rozmiar elementu w tablicy
}

// Nagłówek programu służy do przechowywania informacji dotyczących segmentów programu
type Segment struct {
	Type   uint32 // typ segmentu
	Offset uint32 // offset w pliku, wskazujący na początek danego segmentu
	Vaddr  uint32 // adres wirtualny pod którym w procesie wyląduje pierwszy bajt segmentu
	Paddr  uint32 // jest ważny dla systemów z adresowanie fizycznym
	Filesz uint32 // wielkość w bajtach, zajmowana przez segment w pliku
	Memsz  uint32 // wielkość w bajtach, zajmowana przez segment w pamięci
	Flags  uint32 // flagi segmentu
	Align  uint32 // wielkość w bajtach do której segmenty są wyrównane w pliku oraz pamięci
}

type Symbol struct {
	Name  uint32 // nazwa, jako indeks, wskazujący na string, zakończony zerem w sekcji stringów
	Value uint32 // wartość: pozycja w sekcji lub pamięci
	Size  uint32 // rozmiar (czyli rozmiar zmiennej lub rozmiar kodu funkcji)
	Info  uint8  // typ: zmienna, funkcja, specjalny symbol reprezentujący początek sekcji w odniesieniach
	Other uint8  // to pole obecnie zawiera 0 i nie posiada zdefiniowanego znaczenia
	Shndx uint16 // indeks do tablicy nagłówków sekcji
}

const SymbolSize = 16

type Elf struct {
	Elf                []byte
	Entrypoint         uint32   // wirtualny adres punktu wejścia
	ExecutableSegments uint32   // liczba segmentów wykonywalnych
	VirtualOffset      []uint32 // wirtualne przesunięcie dla konwersji fizyczny > wirtualny adres w segmentach wykonywalnych
	ExecutableOffset   []uint32 // przesunięcie fizyczne segmentów wykonywalnych
	ExecutableLength   []uint32 // rozmiar wykonywalnych segmentów

	Symtab []Symbol // tablicę symboli
	Strtab []byte   // tablica łańcuchów reprezentujących nazwy związane z pozycjami tablicy symboli
}

type Function struct {
	// podstawowe kryteria identyfikacji funkcji
	Name    string   // nazwa funkcji
	Pattern []uint32 // tabela początkowych instrukcji w funkcji
	Mask    []uint32 // tabela z maską dla instrukcji początkowych w funkcji
	Length  uint32   // liczba instrukcji w tabeli instrukcjach wstępnych

	// kryteria potwierdzające identyfikację funkcji
	JalCount uint32 // licznik wystąpień instrukcji JAL w funkcji
	//  jeśli 0, funkcja zostanie zidentyfikowana tylko na podstawie wstępnych instrukcji
	JalScope   uint32  // górna granica szacunkowego rozmiaru funkcji
	JalAddress *uint32 // adres docelowy JAL
	//  jeśli 0, zostanie użyte przesunięcie i tolerancja
	JalRelativeOffset int32 // przesunięcie względne JAL
	//  jeśli 0, cel zostanie zidentyfikowany jako JAL bez potwierdzenia
	JalRelativeOffsetTolerance uint32 // tolerancja dla adresu względnego JAL

	// kryteria identyfikacji instrukcji docelowej (Mastercode)
	TargetOffset    int32 // przesunięcie od początku funkcji docelowej instrukcji
	TargetJalOffset int32 // przesunięcie docelowej instrukcji od JAL

	// wartości robocze
	Counter    uint32 // licznik porównania instrukcji wstępnych
	JalCounter uint32 // licznik wystąpień JAL

	// wartości uzyskane z analizy
	Address       uint32 // adres funkcji
	TargetAddress uint32 // adres zidentyfikowanego celu w ramach funkcji
	Candidates    int32  // liczba dopasowań tylko binarnych
	//  liczba ta jest ustawiana na -1, gdy nastąpi identyfikacja z tablicy symboli (bez dwuznaczności)
	Matches int32 // potwierdzone dopasowania (wartość dodatnia oznacza dwuznaczność kryteriów)
}

type Result struct {
	Type          string // typ wyniku (nazwa funkcji)
	Candidates    int32  // liczba kandydatów
	Matches       uint32 // licznik dopasowań
	TargetAddress uint32 // dane celu
	TargetData    uint32
	Segment       uint32 // segment wykonywalny
}

func GetString(section []byte, start int) string {
	if start < 0 || start >= len(section) {
		return ""
	}

	for end := start; end < len(section); end++ {
		if section[end] == 0 {
			return string(section[start:end])
		}
	}

	return ""
}

func NewElf(r io.Reader) (*Elf, error) {
	result := &Elf{}

	var err error
	result.Elf, err = ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	if bytes.Compare([]byte{0x7f, 0x45, 0x4c, 0x46}, result.Elf[:4]) != 0 { // " ELF"
		return nil, errors.New("magic not matched")
	}

	sr := io.NewSectionReader(bytes.NewReader(result.Elf), 0, 1<<63-1)

	header := new(Header)

	if err := binary.Read(sr, binary.LittleEndian, header); err != nil {
		return nil, err
	}

	segment := make([]*Segment, header.Phnum)
	for i := uint32(0); i < uint32(header.Phnum); i++ {
		off := int64(header.Phoff + i*uint32(header.Phentsize))
		//noinspection GoUnhandledErrorResult
		sr.Seek(off, io.SeekStart)
		ph := new(Segment)
		if err := binary.Read(sr, binary.LittleEndian, ph); err != nil {
			return nil, err
		}
		segment[i] = ph
	}

	section := make([]*Section, header.Shnum)
	for i := uint32(0); i < uint32(header.Shnum); i++ {
		off := int64(header.Shoff + i*uint32(header.Shentsize))
		//noinspection GoUnhandledErrorResult
		sr.Seek(off, io.SeekStart)
		sh := new(Section)
		if err := binary.Read(sr, binary.LittleEndian, sh); err != nil {
			return nil, err
		}
		section[i] = sh
	}

	result.Entrypoint = header.Entry

	// Type == PT_LOAD - ładowalny segment
	// Flags == PF_X - wykonywalny
	for i := uint32(0); i < uint32(header.Phnum); i++ {
		if segment[i].Type == 1 && segment[i].Flags&1 == 1 {
			result.ExecutableSegments++
		}
	}

	result.VirtualOffset = make([]uint32, result.ExecutableSegments)
	result.ExecutableOffset = make([]uint32, result.ExecutableSegments)
	result.ExecutableLength = make([]uint32, result.ExecutableSegments)

	k := 0
	for i := uint32(0); i < uint32(header.Phnum); i++ {
		if segment[i].Type == 1 && segment[i].Flags&1 == 1 {
			result.VirtualOffset[k] = segment[i].Vaddr - segment[i].Offset
			result.ExecutableOffset[k] = segment[i].Offset
			result.ExecutableLength[k] = segment[i].Filesz
			k++
		}
	}

	if header.Shstrndx != 0xffff {
		for i := uint32(0); i < uint32(header.Shnum); i++ {
			name := GetString(result.Elf[section[header.Shstrndx].Offset:], int(section[i].Name))
			if name == ".symtab" {
				symtab := bytes.NewReader(result.Elf[section[i].Offset : section[i].Offset+section[i].Size])
				result.Symtab = make([]Symbol, symtab.Len()/SymbolSize)
				k := 0
				var symbol Symbol
				for symtab.Len() > 0 {
					//noinspection GoUnhandledErrorResult
					binary.Read(symtab, binary.LittleEndian, &symbol)
					result.Symtab[k] = symbol
					k++
				}
			} else if name == ".strtab" {
				result.Strtab = result.Elf[section[i].Offset : section[i].Offset+section[i].Size]
			}
		}
	}

	return result, nil
}

func (elf *Elf) Analyze() ([]Result, error) {
	functions := []Function{
		{Name: "memcpy",
			Length:                     10,
			JalCount:                   0,
			JalScope:                   0,
			JalAddress:                 nil,
			JalRelativeOffset:          0,
			JalRelativeOffsetTolerance: 0,
			TargetOffset:               -1,
			TargetJalOffset:            0,
			Pattern: []uint32{0x0080402d, 0x2cc20020, 0x1440001c, 0x0100182d, 0x00a81025,
				0x3042000f, 0x54400019, 0x24c6ffff, 0x0100382d, 0x78a30000},
			Mask: []uint32{0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
				0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff}},
		{Name: "sceSifSendCmd",
			Length:                     10,
			JalCount:                   1,
			JalScope:                   15,
			JalAddress:                 nil,
			JalRelativeOffset:          -88,
			JalRelativeOffsetTolerance: 8,
			TargetOffset:               -1,
			TargetJalOffset:            0,
			Pattern: []uint32{0x00c0102d, 0x00e0182d, 0x0100582d, 0x27bdfff0, 0x0120502d,
				0x00a0302d, 0xffbf0000, 0x0040382d, 0x0060402d, 0x0160482d},
			Mask: []uint32{0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
				0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff}},
		{Name: "scePadRead",
			Length:                     10,
			JalCount:                   2,
			JalScope:                   30,
			JalAddress:                 nil,
			JalRelativeOffset:          0,
			JalRelativeOffsetTolerance: 0,
			TargetOffset:               -1,
			TargetJalOffset:            0,
			Pattern: []uint32{0x0080382d, 0x24030070, 0x2404001c, 0x70e31818, 0x00a42018,
				0x27bd0000, 0x3c020000, 0xffb00000, 0xffbf0000, 0x24420000},
			Mask: []uint32{0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
				0xffff0000, 0xff000000, 0xff000000, 0xffff0000, 0xffff0000}},
		{Name: "scePad2Read",
			Length:                     10,
			JalCount:                   3,
			JalScope:                   40,
			JalAddress:                 nil,
			JalRelativeOffset:          0,
			JalRelativeOffsetTolerance: 0,
			TargetOffset:               -1,
			TargetJalOffset:            0,
			Pattern: []uint32{0x27bdffc0, 0x24020330, 0xffb10010, 0x3c03003d, 0x0080882d,
				0xffb20020, 0x02222018, 0x2466ff40, 0xffbf0030, 0x00a0902d},
			Mask: []uint32{0xffff0000, 0xffff0000, 0xffff0000, 0xffff0000, 0xff000000,
				0xffff0000, 0xff000000, 0xffff0000, 0xff000000, 0xff000000}},
		{Name: "main",
			Length:                     1,
			JalCount:                   1,
			JalScope:                   100,
			JalAddress:                 nil,
			JalRelativeOffset:          0,
			JalRelativeOffsetTolerance: 0,
			TargetOffset:               -1,
			TargetJalOffset:            0,
			Pattern:                    []uint32{0x0},
			Mask:                       []uint32{0x0}},
		{Name: "entrypoint",
			Length:                     1,
			JalCount:                   0,
			JalScope:                   0,
			JalAddress:                 nil,
			JalRelativeOffset:          0,
			JalRelativeOffsetTolerance: 0,
			TargetOffset:               -1,
			TargetJalOffset:            0,
			Pattern:                    []uint32{0x0},
			Mask:                       []uint32{0x0}},
	}

	for i := 0; i < len(functions); i++ {
		if functions[i].Name == "scePadRead" || functions[i].Name == "scePad2Read" {
			functions[i].JalAddress = &functions[0].Address // memcpy
		}
	}

	result := make([]Result, len(functions))

	if len(elf.Symtab) > 0 && len(elf.Strtab) > 0 {
		for i := 0; i < len(elf.Symtab); i++ {
			if (elf.Symtab[i].Info & 0xf) == 2 { // funkcja
				for k := uint32(0); k < uint32(len(functions)); k++ {
					if functions[k].Address == 0 {
						if GetString(elf.Strtab, int(elf.Symtab[i].Name)) == functions[k].Name {
							functions[k].Address = elf.Symtab[i].Value
							functions[k].Matches = 1
							functions[k].Candidates = -1
							if functions[k].TargetOffset >= 0 {
								functions[k].TargetAddress =
									functions[k].Address + uint32(functions[k].TargetOffset<<2)
							} else if functions[k].JalCount > 0 {
								functions[k].JalScope = elf.Symtab[i].Size >> 2
							}
						}
					}
				}
			}
		}
	}

	for k := uint32(0); k < uint32(len(functions)); k++ {
		if functions[k].Name == "entrypoint" {
			functions[k].Address = elf.Entrypoint
			functions[k].Candidates = -3
		} else if functions[k].Name == "main" {
			if functions[k].Candidates == 0 {
				functions[k].Candidates = -4
			}
		}
	}

	idx := 0
	data := make([]uint32, len(elf.Elf)/4)
	for i := range data {
		data[i] = binary.LittleEndian.Uint32(elf.Elf[i*4 : (i+1)*4])
	}

	var i uint32
	var crc uint32
	for i = 0; i < (elf.ExecutableOffset[0] >> 2); i++ {
		crc ^= data[idx]
		idx++
	}

	for t := uint32(0); t < elf.ExecutableSegments; t++ {
		for ; i < ((elf.ExecutableOffset[t] >> 2) + (elf.ExecutableLength[t] >> 2)); i++ {
			crc ^= data[idx]

			for k := 0; k < len(functions); k++ {
				if functions[k].Candidates >= 0 {
					if functions[k].Counter < functions[k].Length {
						if (data[idx]&functions[k].Mask[functions[k].Counter]) ==
							(functions[k].Pattern[functions[k].Counter]&
								functions[k].Mask[functions[k].Counter]) && functions[k].Length != 1 {
							functions[k].Counter++
						} else {
							functions[k].Counter = 0
						}
					} else if functions[k].Counter < functions[k].JalScope {
						if data[idx] == 0x03e00008 {
							functions[k].Counter = 0
							functions[k].JalCounter = 0
						} else {
							functions[k].Counter++

							if (data[idx] & 0xfc000000) == 0x0c000000 { // JAL
								functions[k].JalCounter++

								if functions[k].JalCounter == functions[k].JalCount {
									if functions[k].JalAddress == nil {
										if functions[k].JalRelativeOffset != 0 {
											difference := (int32(data[idx])&int32(^uint32(0xfc000000)))<<2 -
												int32(uint32(4*idx)+elf.VirtualOffset[t]) -
												(functions[k].JalRelativeOffset << 2)
											if difference < 0 {
												difference = -difference
											}
											difference = difference >> 2
											if uint32(difference) <= functions[k].JalRelativeOffsetTolerance {
												functions[k].Address =
													uint32(4*idx) + elf.VirtualOffset[t] -
														((functions[k].Counter - 1) << 2)
												functions[k].TargetAddress =
													uint32(4*idx) + elf.VirtualOffset[t] +
														uint32(functions[k].TargetJalOffset<<2)
												result[k].Segment = t
												functions[k].Matches++
											}
										} else {
											functions[k].Address =
												uint32(4*idx) + elf.VirtualOffset[t] -
													((functions[k].Counter - 1) << 2)
											functions[k].TargetAddress =
												uint32(4*idx) + elf.VirtualOffset[t] +
													uint32(functions[k].TargetJalOffset<<2)
											result[k].Segment = t
											functions[k].Matches++
										}
									} else {
										functions[k].Address =
											uint32(4*idx) + elf.VirtualOffset[t] +
												((functions[k].Counter - 1) << 2)
										functions[k].TargetAddress =
											uint32(4*idx) + elf.VirtualOffset[t]
										result[k].Segment = t
										functions[k].Matches++
									}

									functions[k].Counter = 0
									functions[k].JalCounter = 0
								}
							}
						}
					} else {
						functions[k].Counter = 0
						functions[k].JalCounter = 0
					}

					if functions[k].Counter == functions[k].Length {
						functions[k].Candidates++

						if functions[k].JalCount == 0 {
							functions[k].Address = uint32(4*idx) - uint32(0) + elf.VirtualOffset[t] -
								((functions[k].Counter - 1) << 2)
							functions[k].Matches++

							if functions[k].TargetOffset >= 0 {
								functions[k].TargetAddress =
									functions[k].Address + uint32(functions[k].TargetOffset<<2)
								result[k].Segment = t
							}

							functions[k].Counter = 0
						}
					}
				} else if functions[k].Candidates >= -2 {
					address := uint32(4*idx) - uint32(0) + elf.VirtualOffset[t]

					if address >= (functions[k].Address + (functions[k].Length << 2)) {
						if address < functions[k].Address+(functions[k].Length<<2)+
							(functions[k].JalScope<<2) {
							result[k].Segment = t

							if functions[k].TargetOffset < 0 && functions[k].JalCount != 0 {
								if data[idx] == 0x03e00008 {
									functions[k].Counter = 1
									functions[k].JalCounter = 0
								} else if (data[idx]&0xfc000000) == 0x0c000000 &&
									functions[k].Counter == 0 { // JAL
									functions[k].JalCounter++

									if functions[k].JalCounter == functions[k].JalCount {
										if functions[k].JalAddress == nil {
											if functions[k].JalRelativeOffset != 0 {
												difference := (int32(data[idx])&int32(^uint32(0xfc000000)))<<2 -
													int32(address) - (functions[k].JalRelativeOffset << 2)
												if difference < 0 {
													difference = -difference
												}
												difference = difference >> 2
												if uint32(difference) <= functions[k].JalRelativeOffsetTolerance {
													functions[k].TargetAddress =
														address + uint32(functions[k].TargetJalOffset<<2)
												}
											} else {
												functions[k].TargetAddress =
													address + uint32(functions[k].TargetJalOffset<<2)
												result[k].Segment = t
											}
										} else {
											functions[k].TargetAddress = address
											result[k].Segment = t
										}
									}
								}
							}
						}
					}
				} else if functions[k].Candidates == -3 {
					if functions[k].Counter == 0 {
						result[k].Segment = t
						address := uint32(4*idx) - uint32(0) + elf.VirtualOffset[t]

						if address >= functions[k].Address {
							if (data[idx] & 0xfc000000) == 0x0c000000 { // JAL
								functions[k].JalCounter++
							}

							if functions[k].JalCounter == 3 {
								functions[k].Counter = 1

								var j int
								for j = 0; j < len(functions); j++ {
									if functions[j].Name == "main" {
										break
									}
								}

								if j != len(functions) {
									if functions[j].Candidates != -1 &&
										address < ((data[idx])&^uint32(0xfc000000))<<2 {
										functions[j].Address = ((data[idx]) &^ uint32(0xfc000000)) << 2
										functions[j].Candidates = -2
									}
								}
							} else if ((data[idx]&0xfc000000) == 0x08000000 && !((((data[idx])&^uint32(0xfc000000))<<2) < address &&
								(((data[idx])&^uint32(0xfc000000))<<2) >= functions[k].Address)) ||
								((data[idx]&0xfc000000) == 0x00000000 && ((data[idx]&0x0000003f) == 0x00000009 ||
									(data[idx]&0x0000003f) == 0x00000008)) {
								functions[k].Counter = 1
							}
						}
					}
				}
			}

			idx++
		}

		if (t + 1) != elf.ExecutableSegments {
			for ; i < (elf.ExecutableOffset[t+1] >> 2); i++ {
				crc ^= data[idx]
				idx++
			}
		} else {
			for ; i < uint32(len(elf.Elf)>>2); i++ {
				crc ^= data[idx]
				idx++
			}
		}
	}

	for k := 0; k < len(functions); k++ {
		if functions[k].Address > 0 && functions[k].TargetAddress > 0 && functions[k].JalCount > 0 &&
			functions[k].JalAddress != nil {
			p := elf.Elf[functions[k].TargetAddress-elf.VirtualOffset[result[k].Segment]:]
			if ((binary.LittleEndian.Uint32(p[:4]) & ^uint32(0xfc000000)) << 2) == *functions[k].JalAddress {
				functions[k].TargetAddress += uint32(functions[k].TargetJalOffset << 2)
			} else {
				functions[k].TargetAddress = 0
			}
		}
	}

	log.Printf("elf crc: %X\n", crc)

	for i = 0; i < uint32(len(result)); i++ {
		result[i].Type = functions[i].Name
		result[i].Candidates = functions[i].Candidates
		result[i].Matches = uint32(functions[i].Matches)
		result[i].TargetAddress = functions[i].TargetAddress
		if result[i].TargetAddress > 0 {
			p := elf.Elf[functions[i].TargetAddress-elf.VirtualOffset[result[i].Segment]:]
			result[i].TargetData = binary.LittleEndian.Uint32(p[:4])
		}
	}

	return result, nil
}
