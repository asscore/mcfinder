package main

import (
	"errors"
	"flag"
	"log"
	"os"

	"mcfinder/elf"
)

func main() {
	var elfName string
	flag.StringVar(&elfName, "elf", "", "*ELF file")
	flag.Parse()

	if elfName == "" {
		log.Fatal(errors.New("elf file argument required"))
	}

	elfFile, err := os.Open(elfName)
	if err != nil {
		log.Fatal(err)
	}
	//noinspection GoUnhandledErrorResult
	defer elfFile.Close()

	ef, err := elf.NewElf(elfFile)
	if err != nil {
		log.Fatal(err)
	}

	results, _ := ef.Analyze()
	for _, result := range results {
		log.Printf("9%07X %08X - type: \"%s\", candidates: %d matches: %d",
			result.TargetAddress, result.TargetData, result.Type, result.Candidates, result.Matches)
	}
}
